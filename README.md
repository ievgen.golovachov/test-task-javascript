### Задача

Вывод текстовой строки в ячейке со строго заданной шириной и ограничением по высоте (2-3 строки).

В случае если текст полностью не помещается, то в конце добавить троеточие (не должно выходить за пределы текста). 

Сделать аналог **-webkit-line-clamp** на чистом JavaScript. 

**Исключить использование DOM для калькуляции.**

```js
/**
 * @param width - Ширина ячейки
 * @param maxLines - Максимальное количество строк
 * @param text - Текст
 * @param font - Размер, семейство шрифта
 */
const clampText = function(width, maxLines, text, font) {
  return 'HELLO';
};
```

Пример: https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-line-clamp
