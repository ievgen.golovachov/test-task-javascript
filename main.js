const font = {
  fontFamily: 'Arial',
  fontSize: 16
};

const ellipsis = '\u2026';

const clampText = function(width, maxLines, text, font) {
  return 'hello';
};

// Testing
const loremIpsumLong = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

const loremIpsumIpsumShort = 'Lorem ipsum dolor sit amet';

const render = function(width, maxLines, text, font) {
  const container = document.getElementById('container');

  container.style.fontFamily = font.fontFamily;
  container.style.fontSize = font.fontSize + 'px';
  container.style.width = width + 'px';
  container.innerHTML = clampText(width, maxLines, text, font);
};

render(300, 3, loremIpsumLong, font);
